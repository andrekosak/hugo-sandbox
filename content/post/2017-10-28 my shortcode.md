---
title: Shortcode
subtitle: Trying to get it work
date: 2017-10-28
tags: ["example", "photoswipe"]
---

### Custom table shortcode

{{<table>}}
| #  | name | notes |
|----|-----|------|
| 1 | Eddy | true |
| 2 | Alice | false |
{{</table>}}
